#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'
require 'tty-prompt'

require_relative 'lib/gitlab_api'
require_relative 'lib/report'
require_relative 'lib/util'

## Configuration parameters

prompt_symbol = '>>'
prompt = TTY::Prompt.new(interrupt: :exit)

# add your groups here
group_selection = ['Composition analysis', 'Static analysis', 'Dynamic analysis', 'Vulnerability Research']

# add label sets here
label_selection = [['feature'], %w[P1 bug]]

# we only consider secure stage at the moment
stage = { 'devops::secure' => 'Secure Stage' }

# add/adjust your question strings here
questions = {
  token: "#{prompt_symbol} Please enter your GITLAB_API_TOKEN?",
  days: "#{prompt_symbol} How many days ago?",
  teams: "#{prompt_symbol} Which team(s)?",
  mrs: "#{prompt_symbol} Are you interested in MRs?",
  group_selection: "#{prompt_symbol} Would you like to filter by group?",
  labels: "#{prompt_symbol} Are you looking for certain labels?",
  label_selection: "#{prompt_symbol} Which labels are you looking for?"
}

# these are the categories to store the data that is retrieved through gitlab
data_md_categories = {
  merged_mrs: 'Merged MRs',
  closed_issues: 'Closed Issues'
}

# Q1: Enter GitLab API token only if it is not set through the environment variable yet
token = if !ENV.key?('GITLAB_API_TOKEN') || ENV['GITLAB_API_TOKEN'].empty?
          prompt.mask(questions[:token]) { |q| q.validate { |input| input =~ /^.+/ } }
        else
          ENV['GITLAB_API_TOKEN']
        end

# Q2: Select the number of days you would like to look int the past
how_many_days_ago = prompt.ask(questions[:days], default: 7) do |q|
  q.required(true)
  q.in('1-100')
end
since = Date.today - how_many_days_ago.to_i

# Q3: Select groups
teams = if prompt.yes?(questions[:group_selection], default: false)
          prompt.multi_select(questions[:teams], cycle: true) do |q|
            q.choices(group_selection)
          end
        else
          []
        end

# Q4/5: Select labels
if prompt.yes?(questions[:labels])
  label_selection_flat = label_selection.flat_map { |labels| labels.join(',') }
  labels = prompt.select(questions[:label_selection], label_selection_flat, cycle: true)
else
  labels = questions[:label_selection]
end

group_selectors_h = teams.map do |group_name|
  group_label = Utils.group_label(group_name)
  ["#{stage.keys.first},#{group_label}" "#{',' unless labels.empty?}#{labels}", group_name]
end.to_h

# treat devops stage as a group
group_selectors_h.merge!(stage) if group_selectors_h.empty?

# Q6: MRs and or issues
mrs = prompt.yes?(questions[:mrs])

data = {}

## Data collection
begin
  report = Report.new(token)

  group_selectors_h.each do |group_selector, group_name|
    prompt.say("Gathering data for #{group_name} ...")
    retrieved_issues = report.get_past_week_issues(group_selector, since)

    issue_links = retrieved_issues.map do |issue|
      Utils.markdown_link(issue['title'], issue['web_url'])
    end

    if issue_links.any?
      closed_issues_md = data_md_categories[:closed_issues]
      data[closed_issues_md] = {} if data[closed_issues_md].nil?
      data[closed_issues_md][group_name] = [] unless data[closed_issues_md].key?(group_name)
      data[closed_issues_md][group_name] = issue_links
    end

    next unless mrs

    retrieved_mrs = report.get_past_week_mrs(group_selector, since)
    mr_links = retrieved_mrs.map do |mr|
      Utils.markdown_link(mr['title'], mr['web_url'])
    end

    next unless mr_links.any?

    merged_mrs_md = data_md_categories[:merged_mrs]
    data[merged_mrs_md] = {} if data[merged_mrs_md].nil?
    data[merged_mrs_md][group_name] = [] unless data[merged_mrs_md].key?(group_name)
    data[merged_mrs_md][group_name] = mr_links
  end

  # Generate markdown report
  markdown_compiler = ERB.new(File.read('resources/md_template.erb'))
  markdown_s = markdown_compiler.result_with_hash(data_map: data)

  prompt.say("writing results to report_#{since}.md")
  File.open("report_#{since}.md", 'w') do |file|
    file.write(markdown_s)
  end || raise(StandardError)
rescue GitlabApi::ConnectionFailed, StandardError => e
  puts(e.message)
  exit(1)
end
