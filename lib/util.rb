# frozen_string_literal: true

require 'erb'

# adds a whitelisting function to strings (used for markdown link names)
class String
  def strip_special
    gsub(%r{[^0-9A-Za-z \\/:;,\-+]}, '')
  end
end

# some useful utilty functions
module Utils
  def self.compute_http_params(dict)
    dict.map { |k, v| "#{k}=#{v}" }.join('&')
  end

  def self.group_label(name)
    "group::#{ERB::Util.url_encode(name.downcase)}"
  end

  def self.markdown_link(name, url)
    "[#{name.strip_special}](#{url})"
  end
end
