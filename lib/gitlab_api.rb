# frozen_string_literal: true

require 'httparty'

# Helper class to communicate with the GitLab API
class GitlabApi
  ENDPOINT = 'https://gitlab.com/api/v4'
  MAX_PAGES = 5

  ConnectionFailed = Class.new(StandardError)

  def initialize(token: nil)
    @token = token
  end

  def get(path, updated_after, auth: false)
    path = "#{ENDPOINT}/#{path}" unless path.start_with?(ENDPOINT)

    options = {}
    options[:headers] = { 'Private-Token': token } if auth
    data = []

    MAX_PAGES.times do |page|
      HTTParty.get("#{path}&per_page=100&page=#{page}&updated_after=#{updated_after}", options).tap do |response|
        raise ConnectionFailed if response.code.to_i != 200

        payload = JSON.parse(response.body)
        next if payload.empty?

        data += payload
      end
    end

    data
  end

  private

  attr_reader :token
end
