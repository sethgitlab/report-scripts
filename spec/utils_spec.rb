#!/usr/bin/env ruby
# frozen_string_literal: true

require 'rspec'
require_relative '../lib/util'

describe Utils do
  context 'utilitiles' do
    it 'should escape markdown links' do
      expect(Utils.markdown_link('name````', 'http://test') == '[name](http://test)')
      expect(Utils.markdown_link('test````<>""""', 'http://test') == '[test](http://test)')
    end

    it 'should generate proper group labels' do
      expect(Utils.group_label('test') == 'group::test')
    end

    it 'should compute proper http params' do
      expect(Utils.compute_http_params('a': 'b', 'c': 'd') == 'a=b&c=d')
    end
  end
end
